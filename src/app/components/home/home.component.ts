import { Component, OnInit } from '@angular/core';
import { QuickBloxService } from 'src/shared/quickblox.service';

declare var QB: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public usersList = [];
  public session;
  public accept:boolean = false;
  public callSession:boolean = false;

  constructor(private QB: QuickBloxService) { }

  ngOnInit() {
    this.session = JSON.parse(localStorage.getItem('session'));
    console.log('Session:', this.session.user_id);
    QB.users.listUsers( (error, response)=>{
        if(error){
          console.log("Get Users Error: ", error);
        } else {
          this.usersList = response['items'].filter((user) => {
              return user.user.id != this.session.user_id;
          })
          console.log("Users List: ", this.usersList);
        }
    });

    this.activateListeners();
  }

  acceptCall(){
    this.callSession = true;
  }

  activateListeners(){
    QB.webrtc.onCallListener = function(session, extension) {
      console.log('onCallListerner ==== ',session)
      this.accept = true;
      if(this.callSession){
        session.accept({});
      }
    }
    QB.webrtc.onAcceptCallListener = function(session, userId, extension) {
        console.log('onAcceptCallListener === ', session, userId);
        this.getRecipientMedia(session);
    }; 
      QB.webrtc.onRemoteStreamListener = function(session, userID, remoteStream) {
          session.attachMediaStream('remoteVideo', remoteStream);
      };
  }

  getRecipientMedia(session){
    var mediaParams = {
      audio: true,
      video: true,
      options: {
          muted: true,
          mirror: true
      },
      elemId: 'localVideo'
    };

    session.getUserMedia(mediaParams, function(err, stream) {
        if (err){
            console.log('usermedia error ===== ', err);
        }else{
            console.log('Stream ===== ', stream);
            session.attachMediaStream('localVideo', stream);
            var extension = {};
            session.call(extension, function(error) {
                if(error) {
                    console.log('Call === ', error);
                } else {
                    console.log(extension);
                }   
            });
        }
    });
  }

  onUserSelect(user){
    console.log(user);
    var calleesIds = [user];
    var sessionType = QB.webrtc.CallType.VIDEO;
    var callerID = this.session.user_id;

    var session = QB.webrtc.createNewSession(calleesIds, sessionType, callerID);
    console.log('Session ===== ', session);

    var mediaParams = {
      audio: true,
      video: true,
      options: {
          muted: true,
          mirror: true
      },
      elemId: 'localVideo'
    };

    

    session.getUserMedia(mediaParams, function(err, stream) {
        if (err){
            console.log('usermedia error ===== ', err);
        }else{
            console.log('Stream ===== ', stream);
            session.attachMediaStream('localVideo', stream);
            var extension = {};
            session.call(extension, function(error) {
                if(error) {
                    console.log('Call === ', error);
                } else {
                    console.log(extension);
                }   
            });
        }
    });
   
  }

}
