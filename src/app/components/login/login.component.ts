import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuickBloxService } from 'src/shared/quickblox.service';

declare var QB:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router:Router, 
    private QB: QuickBloxService ){}

  private loginPayload;
  private sessionResponse;

  ngOnInit(){

    this.loginPayload = {
      login: '',
      password: ''
    }
    //Initialize QuickBlox SDK   
    this.QB.initializeQBSDK();
    
  }

  onLoginSubmit(){
    var loginPayload = this.loginPayload;
    var router = this.router;
    QB.createSession(this.loginPayload, function(err, result) {
      if(err){
        console.log(err);
      } else{
          QB.chat.connect({userId: result.user_id, password: loginPayload.password}, function(err, roster) {
              if(err){
                  console.log('====>Chat Connection Error', err);
              }else{
                  console.log('====>Chat connected: ', result);
                  localStorage.setItem('session', JSON.stringify(result));
                  router.navigate(['home']);
              }
          });
      }
  });
  }

}
