import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppConstants } from './app.constants';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RouterModule } from '@angular/router';
import { QuickBloxService } from 'src/shared/quickblox.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
  BrowserModule,
  FormsModule,
  RouterModule.forRoot([
    {
      path: '',
      component: LoginComponent
    },
    {
      path: 'home',
      component: HomeComponent
    }
  ])
  ],
  providers: [AppConstants, QuickBloxService],
  bootstrap: [AppComponent]
})
export class AppModule { }
