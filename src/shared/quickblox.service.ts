import { Injectable,LOCALE_ID } from '@angular/core';
import { AppConstants } from '../app/app.constants'
import { Observable } from 'rxjs';

export declare var QB: any;

@Injectable()
export class QuickBloxService {
    constructor(private appConstants: AppConstants) {}

    private CREDENTIALS = this.appConstants.CREDENTIALS;

    initializeQBSDK(){
        return QB.init(this.CREDENTIALS.appId, this.CREDENTIALS.authKey, this.CREDENTIALS.authSecret);
    }

}